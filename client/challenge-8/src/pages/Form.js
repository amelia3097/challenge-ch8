import { Component, useState } from "react"
import { useNavigate } from "react-router-dom"

function Form() {
    const navigate= useNavigate()
    const [username,setUsername] = useState()
    const [password,setPassword] = useState()
    const [email,setEmail] = useState()

   
   const onSubmit = (e) => {
        e.preventDefault()
        fetch("http://localhost:5000/api/players", {
            method: "POST",
            headers: {
                "content-type": "application/json"
            },
            body: JSON.stringify({
                username:username,
                password:password,
                email:email
            })
        })
            .then(navigate('/'))
    }


    return (
        <>
            <h1 className="text-center mb-5">Creat New Player</h1>
            <div className="row mx-5">
                <div className="col-md-8 justify-content-center">
                    <form className="" onSubmit={onSubmit}>
                        <div className="input-group">
                            <span className="input-group-text">username</span>
                            <input value={username} type="text"
                                onChange={e => setUsername(e.target.value)} placeholder="username" />
                        </div>
                        <div className="input-group">
                            <span className="input-group-text">password</span>
                            <input value={password} type="password"
                                onChange={e => setPassword(e.target.value)} placeholder="password" />
                        </div>
                        <div className="input-group">
                            <span className="input-group-text">email</span>

                            <input value={email} type="email"
                                onChange={e => setEmail(e.target.value)} placeholder="email" />
                        </div>
                        <button className="mt-5" type="submit">Save</button>
                    </form>
                </div>
            </div>
        </>
    )
}


export default Form