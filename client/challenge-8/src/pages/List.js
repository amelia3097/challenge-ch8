import { Component } from "react";
import { Link } from "react-router-dom";

class List extends Component {
    state = {
        players: []
    }

    componentDidMount() {
        fetch("http://localhost:5000/api/players")
            .then(res => res.json())
            .then(data => {
                this.setState({ players: [...data.message] })
            })
    }

    deletePlayer(e, id){
        e.preventDefault()
        fetch(`http://localhost:5000/api/players/${id}`, {
            method: "DELETE",
            headers: {
                "content-type": "application/json"
            },
        })
        .then(window.location.reload())
    }

    renderPlayer = player => {
        return (
            <tr>
                        <th scope="row">{player.id}</th>
                            <td>{player.username}</td>
                            <td>{player.email}</td>
                            <td>{player.password}</td>
                            <td>{player.experience}</td>
                            <td>{player.lvl}</td>
                            <td><a href={`/edit/${player.id}`}>Edit</a></td>
                            <td><a href="#" onClick={ e => this.deletePlayer(e, player.id)}>Delete</a></td>
                        </tr>
        )
    }

    render() {
        return (
            <>
                <Link to="/form" className="btn btn-primary">Create new player</Link>
                <h1>Player List</h1>
                <ul>
                   
                </ul>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Username</th>
                            <th scope="col">Email</th>
                            <th scope="col">Password</th>
                            <th scope="col">Experience</th>
                            <th scope="col">lvl</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.players.map(this.renderPlayer)}
                        
                    </tbody>
                </table>
            </>
        )
    }
}

export default List