import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'

function Edit() {
    const navigate= useNavigate()
    const param= useParams()
    const [username,setUsername] = useState()
    const [password,setPassword] = useState()
    const [email,setEmail] = useState()
    const [experience,setExperience] = useState()
    const [lvl,setLvl] = useState()

   const onSubmit = (e) => {
        e.preventDefault()
        fetch(`http://localhost:5000/api/players/${param.id}`, {
            method: "PUT",
            headers: {
                "content-type": "application/json"
            },
            body: JSON.stringify({
                username:username,
                password:password,
                email:email,
                experience:experience,
                lvl:lvl
            })
        })
            .then(navigate('/'))
    }

    const getData= () => {
        fetch(`http://localhost:5000/api/players/${param.id}`)
        .then(res => res.json())
        .then(res => {
            // console.log(res)
            setUsername(res.message.username)
            setPassword(res.message.password)
            setEmail(res.message.email)
            setExperience(res.message.experience)
            setLvl(res.message.lvl)
        })
    }
    useEffect(() => {

    getData()
      
    }, [])
    

    return (
        <>
            <h1 className="text-center mb-5">Edit Player</h1>
            <div className="row mx-5">
                <div className="col-md-8 justify-content-center">
                    <form className="" onSubmit={onSubmit}>
                        <div className="input-group">
                            <span className="input-group-text">username</span>
                            <input value={username} type="text"
                                onChange={e => setUsername(e.target.value)} placeholder="username" />
                        </div>
                        <div className="input-group">
                            <span className="input-group-text">password</span>
                            <input value={password} type="password"
                                onChange={e => setPassword(e.target.value)} placeholder="password" />
                        </div>
                        <div className="input-group">
                            <span className="input-group-text">email</span>

                            <input value={email} type="email"
                                onChange={e => setEmail(e.target.value)} placeholder="email" />
                        </div>
                        <div className="input-group">
                            <span className="input-group-text">experience</span>

                            <input value={experience} type="number"
                                onChange={e => setExperience(e.target.value)} placeholder="experience" />
                        </div>
                        <div className="input-group">
                            <span className="input-group-text">lvl</span>

                            <input value={lvl} type="number"
                                onChange={e => setLvl(e.target.value)} placeholder="lvl" />
                        </div>

                        <button className="mt-5" type="submit">Save</button>
                    </form>
                </div>
            </div>
        </>
    )
}

export default Edit